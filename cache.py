#!/usr/bin/python3

# ESTE ES EL MODULO QUE IMPLEMENTA LA CLASE CACHE

from robot import Robot


class Cache:

    # Inicialización de las variables de la Clase Cache
    def __init__(self):
        self.cache = {}  # Diccionario vacío, donde guardaremos las url que queramos descargar

    # Método para guardar las URLs proporcionadas en la caché
    def retrieve(self, url):
        if url not in self.cache:
            robot = Robot(url=url)
            self.cache[url] = robot
        else:
            print("el documento ya ha sido descargado")

    # Método para devolver el contenido de la URL almacenada en la caché
    def content(self, url):
        self.retrieve(url)
        return self.cache[url].content()

    # Método para mostrar el contenido de la URL almacenada en la caché
    def show(self, url):
        print(self.content(url))

    # Método para mostrar todas las URLs almacenadas en la caché
    def show_all(self):
        for url in self.cache:
            print(url)
