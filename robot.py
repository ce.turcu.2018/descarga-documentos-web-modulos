#!/usr/bin/python3

# ESTE ES EL MODULO QUE IMPLEMENTA LA CLASE ROBOT
import urllib.request


class Robot:

    # Inicialización de las variables de la clase Robot
    def __init__(self, url):
        self.url = url  # Esta es la variable de nuestro objeto que contiene la url
        self.retrieved = False  # Esta es la variable de nuestro objeto que identifica si se ha descargado anteriormente
        print(self.url)

    def retrieve(self):
        if self.retrieved == False:  # en caso de que el documento no se haya descargado antes...
            print("Descargando url...")
            f = urllib.request.urlopen(self.url)  # abrimos la url y la guardamos en una variable local
            self.content = f.read().decode('utf-8')  # leemos el contenido de la url y lo guardamos en
                                                     # el atributo content del objeto
            self.retrieved = True  # cambiamos el atributo retrieved a TRUE para indicar que ya ha sido descargado

    # Contenido descargado de la url
    def content(self):
        self.retrieve()
        return self.content

    # Imprimir el contenido de la url, es decir, del documento Web
    def show(self):
        print(self.content())
