#!/usr/bin/python3

# ESTE ES EL MODULO QUE IMPLEMENTA EL PROGRAMA PRINCIPAL DONDE PROBAREMOS LAS CLASES ANTERIORES

from robot import Robot
from cache import Cache

print("Test Robot class")
r1 = Robot('http://gsyc.urjc.es/')
r2 = Robot('https://www.python.org/downloads/')

r1.show()
r1.retrieve()
r1.retrieve()  # Como vemos, el mensaje aparece una sola vez, sólo descargamos 1 vez
r2.show()
r2.retrieve()

print("Test Cache class")
c1 = Cache()
c2 = Cache()

c1.retrieve('http://gsyc.urjc.es/')
c2.retrieve('https://www.python.org/downloads/')
c1.show('https://www.aulavirtual.urjc.es')
c2.show('https://www.python.org/downloads/')
c1.show_all()
c2.show_all()
